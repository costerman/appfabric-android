package com.mrosterman.appfabric;

import android.app.Application;

import com.mrosterman.appfabric.notification.NotificationManager;
import com.squareup.otto.Bus;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by carl on 4/12/16.
 */
public class AppFabricBase extends Application {

    private Bus mBus;
    private NotificationManager mNotificationManager;
    private Retrofit mRetrofit;

    private static AppFabricBase mInstance;

    private void init(){
        mBus = new Bus();
        mRetrofit = new Retrofit
                .Builder()
                .baseUrl("http://owinselfhost-dev.us-west-2.elasticbeanstalk.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mNotificationManager = NotificationManager.getInstance();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if(mInstance == null) {
            mInstance = new AppFabricBase();
            mInstance.init();
        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static Bus getBus() { return mInstance.mBus; }

    public static  Retrofit getRetrofit() { return mInstance.mRetrofit; }

}
