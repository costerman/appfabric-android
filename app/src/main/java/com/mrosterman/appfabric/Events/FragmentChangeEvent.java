package com.mrosterman.appfabric.events;

import android.support.v4.app.Fragment;

import com.mrosterman.appfabric.R;

/**
 * Created by carl on 4/18/16.
 */
public class FragmentChangeEvent {

    public int ContainerViewId;

    public Fragment Fragment;

    public String FragmentTag;

    public String BackStackStateName;

    public FragmentChangeEvent(Fragment fragment, String fragmentTag){
        this.ContainerViewId = R.id.fragment_container;
        this.Fragment = fragment;
        this.FragmentTag = fragmentTag;
        this.BackStackStateName = null;
    }
}
