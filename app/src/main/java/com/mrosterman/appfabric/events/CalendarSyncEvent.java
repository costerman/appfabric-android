package com.mrosterman.appfabric.events;

import com.google.gson.annotations.SerializedName;

/**
 * Created by carl on 4/19/16.
 */
public class CalendarSyncEvent {

    @SerializedName("Id")
    public int Id;

    @SerializedName("Type")
    public String Type;
}
