package com.mrosterman.appfabric.events;

import android.support.annotation.Nullable;
import android.view.MenuItem;

/**
 * Created by carl on 4/19/16.
 */
public class ToolbarChangeEvent {

    public ButtonType Type;
    public boolean Enabled;
    public MenuItem.OnMenuItemClickListener ClickListener;

    public enum ButtonType{
        CREATE
    }

    public ToolbarChangeEvent(){}
    public ToolbarChangeEvent(ButtonType type, boolean enabled, @Nullable MenuItem.OnMenuItemClickListener listener){
        this.Type = type;
        this.Enabled = enabled;
        this.ClickListener = listener;
    }
}
