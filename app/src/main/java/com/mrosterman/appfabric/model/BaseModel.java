package com.mrosterman.appfabric.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by carl on 4/19/16.
 */
public class BaseModel {

    @SerializedName("Id")
    public int Id;

}
