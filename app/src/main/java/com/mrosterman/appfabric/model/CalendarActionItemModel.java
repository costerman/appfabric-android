package com.mrosterman.appfabric.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by carl on 4/18/16.
 */
public class CalendarActionItemModel extends BaseModel implements Parcelable {

    @SerializedName("ActionItemName")
    public String ActionItemName;

    @SerializedName("CalendarCategory")
    public String CalendarCategory;

    @SerializedName("ActionItemDescription")
    public String ActionItemDescription;

    @SerializedName("ActionItemDueDate")
    public String ActionItemDueDate;

    @SerializedName("AssignedBy")
    public String AssignedBy;

    @SerializedName("PriorityLevel")
    public String PriorityLevel;

    @SerializedName("PrimaryOwners")
    public String[] PrimaryOwners;

    @SerializedName("NotifyOwnersImmediately")
    public boolean NotifyOwnersImmediately;

    @SerializedName("ActionTaken")
    public String ActionTaken;

    @SerializedName("ActionItemStatus")
    public boolean ActionItemStatus;

    public CalendarActionItemModel(){ }

    protected CalendarActionItemModel(Parcel in) {
        Id = in.readInt();
        ActionItemName = in.readString();
        CalendarCategory = in.readString();
        ActionItemDescription = in.readString();
        ActionItemDueDate = in.readString();
        AssignedBy = in.readString();
        PriorityLevel = in.readString();
        NotifyOwnersImmediately = in.readByte() != 0x00;
        ActionTaken = in.readString();
        ActionItemStatus = in.readByte() != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(ActionItemName);
        dest.writeString(CalendarCategory);
        dest.writeString(ActionItemDescription);
        dest.writeString(ActionItemDueDate);
        dest.writeString(AssignedBy);
        dest.writeString(PriorityLevel);
        dest.writeByte((byte) (NotifyOwnersImmediately ? 0x01 : 0x00));
        dest.writeString(ActionTaken);
        dest.writeByte((byte) (ActionItemStatus ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CalendarActionItemModel> CREATOR = new Parcelable.Creator<CalendarActionItemModel>() {
        @Override
        public CalendarActionItemModel createFromParcel(Parcel in) {
            return new CalendarActionItemModel(in);
        }

        @Override
        public CalendarActionItemModel[] newArray(int size) {
            return new CalendarActionItemModel[size];
        }
    };
}
