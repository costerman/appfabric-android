package com.mrosterman.appfabric.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by carl on 4/19/16.
 */
public class CreateResponseModel extends BaseModel {

    @SerializedName("Type")
    public String Type;

}
