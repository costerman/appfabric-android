package com.mrosterman.appfabric.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by carl on 4/21/16.
 */
public class DeleteResponseModel extends BaseModel {
    @SerializedName("Type")
    public String Type;
}
