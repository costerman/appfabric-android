package com.mrosterman.appfabric.notification;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.noveogroup.android.log.LoggerManager;

import java.net.URISyntaxException;

/**
 * Created by carl on 4/18/16.
 */
public class NotificationManager {

    private Socket mSocket;
    private static NotificationManager mInstance;

    private NotificationManager(){
        try {
            //mSocket = IO.socket("http://10.22.3.163:8080");
            mSocket = IO.socket("http://sync-dev.us-west-2.elasticbeanstalk.com");
            mSocket.connect();
        } catch (URISyntaxException e) {
            LoggerManager.getLogger().e("Failed to connect!", e);
        }
    }

    public static NotificationManager getInstance(){
        if(mInstance == null){
            mInstance = new NotificationManager();
        }
        return mInstance;
    }

    public void listenOnChannel(Channel channel, Emitter.Listener listener){
        mSocket.emit("join-channel", channel.toString());
        mSocket.on(channel.toString(), listener);
    }

    public void removeListener(Channel channel, Emitter.Listener listener){
        mSocket.off(channel.toString(), listener);
    }

    public void emitOnChannel(Channel channel, Object msg){
        mSocket.emit("calendar", msg);
    }

}
