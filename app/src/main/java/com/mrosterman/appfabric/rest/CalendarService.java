package com.mrosterman.appfabric.rest;

import com.mrosterman.appfabric.model.CalendarActionItemModel;
import com.mrosterman.appfabric.model.CreateResponseModel;
import com.mrosterman.appfabric.model.DeleteResponseModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by carl on 4/18/16.
 */
public interface CalendarService {
    @GET("calendar/actionitem")
    Call<List<CalendarActionItemModel>> listActionItems();

    @POST("calendar/actionitem")
    Call<CreateResponseModel> createActionItem(@Body CalendarActionItemModel model);

    @GET("calendar/actionitem/{id}")
    Call<CalendarActionItemModel> getActionItem(@Path("id") int id);

    @DELETE("calendar/actionitem/{id}")
    Call<DeleteResponseModel> deleteActionItem(@Path("id") int id);
}
