package com.mrosterman.appfabric.ui;


import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;

import com.mrosterman.appfabric.AppFabricBase;
import com.mrosterman.appfabric.events.FragmentChangeEvent;
import com.mrosterman.appfabric.R;
import com.mrosterman.appfabric.events.ToolbarChangeEvent;
import com.noveogroup.android.log.LoggerManager;
import com.squareup.otto.Subscribe;


public class MainActivity extends MainActivityBase {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppFabricBase.getBus().register(this);
    }

    @Override
    protected void onPause() {
        AppFabricBase.getBus().unregister(this);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        LoggerManager.getLogger().d("MainActivity onSaveInstanceState()");
        super.onSaveInstanceState(outState);
    }

    @Subscribe
    public void toolbarChangeEvent(ToolbarChangeEvent event){
        if(event.Type == ToolbarChangeEvent.ButtonType.CREATE){
            Menu menu = mToolbar.getMenu();
            MenuItem item = menu.findItem(R.id.action_create);
            if(item != null) {
                item.setVisible(event.Enabled);
                item.setOnMenuItemClickListener(event.ClickListener);
            }
        }
    }

    @Subscribe
    public void fragmentChangeEvent(FragmentChangeEvent event){
        replaceFragment(event.ContainerViewId, event.Fragment, event.FragmentTag, event.BackStackStateName);
    }

}
