package com.mrosterman.appfabric.ui.calendar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.mrosterman.appfabric.AppFabricBase;
import com.mrosterman.appfabric.R;
import com.mrosterman.appfabric.events.FragmentChangeEvent;
import com.mrosterman.appfabric.events.ToolbarChangeEvent;
import com.mrosterman.appfabric.model.CalendarActionItemModel;
import com.mrosterman.appfabric.model.CreateResponseModel;
import com.mrosterman.appfabric.rest.CalendarService;
import com.mrosterman.appfabric.util.EditTextUtil;
import com.mrosterman.appfabric.util.StringUtil;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by carl on 4/12/16.
 */
public class DetailFragment extends Fragment {

    public static final String FRAGMENT_TAG = Fragment.class.getName();
    public static final String BUNDLE_KEY = Fragment.class.getSimpleName() + "DETAIL_BUNDLE_KEY";

    private CalendarService mService;
    private Logger mLogger = LoggerManager.getLogger();

    private EditText mActionItemName;
    private EditText mCalendarCategoryDetail;
    private EditText mActionItemDescription;
    private EditText mDueDate;
    private EditText mAssignedBy;
    private EditText mPriority;
    private EditText mPrimaryOwners;
    private EditText mNotifyOwners;
    private EditText mActionTaken;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mService = AppFabricBase.getRetrofit().create(CalendarService.class);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_calendar_detail, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Set Class Level View Objects
        mActionItemName = (EditText) getActivity().findViewById(R.id.action_item_name_detail_text);
        mActionItemName.setOnFocusChangeListener(EditTextUtil.hideKeyboardOnFocusChange(getActivity(), mActionItemName.getId()));

        mCalendarCategoryDetail = (EditText) getActivity().findViewById(R.id.calendar_category_detail_text);
        mCalendarCategoryDetail.setOnFocusChangeListener(EditTextUtil.hideKeyboardOnFocusChange(getActivity(), mCalendarCategoryDetail.getId()));

        mActionItemDescription = (EditText) getActivity().findViewById(R.id.action_item_desc_detail_text);
        mActionItemDescription.setOnFocusChangeListener(EditTextUtil.hideKeyboardOnFocusChange(getActivity(), mActionItemDescription.getId()));

        mDueDate = (EditText) getActivity().findViewById(R.id.due_date_detail_text);
        mDueDate.setOnFocusChangeListener(EditTextUtil.hideKeyboardOnFocusChange(getActivity(), mDueDate.getId()));

        mAssignedBy = (EditText) getActivity().findViewById(R.id.assigned_by_detail_text);
        mAssignedBy.setOnFocusChangeListener(EditTextUtil.hideKeyboardOnFocusChange(getActivity(), mAssignedBy.getId()));

        mPriority = (EditText) getActivity().findViewById(R.id.priority_detail_text);
        mPriority.setOnFocusChangeListener(EditTextUtil.hideKeyboardOnFocusChange(getActivity(), mPriority.getId()));

        mPrimaryOwners = (EditText) getActivity().findViewById(R.id.primary_owners_detail_text);
        mPrimaryOwners.setOnFocusChangeListener(EditTextUtil.hideKeyboardOnFocusChange(getActivity(), mPrimaryOwners.getId()));

        mNotifyOwners = (EditText) getActivity().findViewById(R.id.notify_owners_detail_text);
        mNotifyOwners.setOnFocusChangeListener(EditTextUtil.hideKeyboardOnFocusChange(getActivity(), mNotifyOwners.getId()));

        mActionTaken = (EditText) getActivity().findViewById(R.id.action_taken_detail_text);
        mActionTaken.setOnFocusChangeListener(EditTextUtil.hideKeyboardOnFocusChange(getActivity(), mActionTaken.getId()));

        Bundle bundle = this.getArguments();
        if(bundle != null) {
            CalendarActionItemModel model = bundle.getParcelable(BUNDLE_KEY);
            updateViewData(model);
            AppFabricBase.getBus().post(new ToolbarChangeEvent(ToolbarChangeEvent.ButtonType.CREATE, false, null));
        } else {
            AppFabricBase.getBus().post(new ToolbarChangeEvent(ToolbarChangeEvent.ButtonType.CREATE, true, createClickListener));

        }
    }

    @Override
    public void onStop() {
        mLogger.d("DetailFragment onStop()");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        mLogger.d("DetailFragment onDestroy()");
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mLogger.d("DetailFragment onSaveInstanceState()");
        super.onSaveInstanceState(outState);
    }

    private MenuItem.OnMenuItemClickListener createClickListener = new MenuItem.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            CalendarActionItemModel model = buildModel();
            Call<CreateResponseModel> result = mService.createActionItem(model);
            result.enqueue(new Callback<CreateResponseModel>() {
                @Override
                public void onResponse(Call<CreateResponseModel> call, Response<CreateResponseModel> response) {
                    //TODO: Something with the response
                    mLogger.d("Response Data: %s", response.body());
                    AppFabricBase.getBus().post(new FragmentChangeEvent(new ListFragment(), ListFragment.FRAGMENT_TAG));
                }

                @Override
                public void onFailure(Call<CreateResponseModel> call, Throwable t) {
                    mLogger.e("Failed create new action item.", t);
                }
            });
            return false;
        }
    };

    private void updateViewData(CalendarActionItemModel model){
        mActionItemName.setText(model.ActionItemName);
        mCalendarCategoryDetail.setText(model.CalendarCategory);
        mActionItemDescription.setText(model.ActionItemDescription);
        mDueDate.setText(model.ActionItemDueDate);
        mAssignedBy.setText(model.AssignedBy);
        mPriority.setText(model.PriorityLevel);
        mPrimaryOwners.setText(StringUtil.concatArray(model.PrimaryOwners));
        mNotifyOwners.setText(model.NotifyOwnersImmediately ? "true" : "false");
        mActionTaken.setText(model.ActionTaken);
    }

    private CalendarActionItemModel buildModel(){

        CalendarActionItemModel model = new CalendarActionItemModel();
        model.ActionItemName = mActionItemName.getText().toString();
        model.CalendarCategory = mCalendarCategoryDetail.getText().toString();
        model.ActionItemDescription = mActionItemDescription.getText().toString();
        model.ActionItemDueDate = mDueDate.getText().toString();
        model.AssignedBy = mAssignedBy.getText().toString();
        model.PriorityLevel = mPriority.getText().toString();
        model.PrimaryOwners = StringUtil.delimitedToArray(",", mPrimaryOwners.getText().toString());
        model.NotifyOwnersImmediately = mNotifyOwners.getText().toString().equals("true") ? true : false;
        model.ActionTaken = mActionTaken.getText().toString();

        return model;
    }
}
