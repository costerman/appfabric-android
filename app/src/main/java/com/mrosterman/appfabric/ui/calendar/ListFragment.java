package com.mrosterman.appfabric.ui.calendar;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.nkzawa.emitter.Emitter;
import com.google.gson.Gson;
import com.mrosterman.appfabric.AppFabricBase;
import com.mrosterman.appfabric.events.CalendarSyncEvent;
import com.mrosterman.appfabric.events.FragmentChangeEvent;
import com.mrosterman.appfabric.R;
import com.mrosterman.appfabric.events.ToolbarChangeEvent;
import com.mrosterman.appfabric.model.CalendarActionItemModel;
import com.mrosterman.appfabric.model.DeleteResponseModel;
import com.mrosterman.appfabric.notification.Channel;
import com.mrosterman.appfabric.notification.NotificationManager;
import com.mrosterman.appfabric.rest.CalendarService;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by carl on 4/12/16.
 */
public class ListFragment extends Fragment implements RecyclerAdapter.AdapterListener {

    public static final String FRAGMENT_TAG = Fragment.class.getName();
    public static final String BUNDLE_KEY = Fragment.class.getSimpleName() + "LIST_BUNDLE_KEY";

    private RecyclerAdapter mRecyclerAdapter;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;

    private TextView mTextView;
    private CalendarService mService;
    private Logger mLogger = LoggerManager.getLogger();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLogger.d("ListFragment onCreate()");
        mService = AppFabricBase.getRetrofit().create(CalendarService.class);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mLogger.d("ListFragment onCreateView()");
        return inflater.inflate(R.layout.fragment_calendar_list, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        mRecyclerAdapter = new RecyclerAdapter(new ArrayList<CalendarActionItemModel>(), this);
        mRecyclerView = (RecyclerView) getActivity().findViewById(R.id.calendar_list_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this.getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mRecyclerAdapter);
        RecyclerHelper.setUpItemTouchHelper(this.getContext(), mRecyclerView);
        RecyclerHelper.setUpAnimationDecoratorHelper(mRecyclerView);

        mTextView = (TextView) getActivity().findViewById(R.id.no_data_label);

        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(fabOnClick);

        if(savedInstanceState != null && savedInstanceState.size() > 0){
            for(int i = 0; i < savedInstanceState.size(); i++){
                String key = String.format("%d-%s", i, BUNDLE_KEY);
                mRecyclerAdapter.add((CalendarActionItemModel) savedInstanceState.get(key));
            }
            mRecyclerAdapter.notifyDataSetChanged();
        }


        NotificationManager.getInstance().listenOnChannel(Channel.calendar, onNewMessage);

        //TODO: Fix rotation issue -> 'android.view.MenuItem android.view.MenuItem.setVisible(boolean)' on a null object reference
        AppFabricBase.getBus().post(new ToolbarChangeEvent(ToolbarChangeEvent.ButtonType.CREATE, false, null));

        Call<List<CalendarActionItemModel>> result = mService.listActionItems();
        result.enqueue(listCallback);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mLogger.d("ListFragment onSaveInstanceState()");

        if(mRecyclerAdapter != null) {
            for (int i = 0; i < mRecyclerAdapter.getItemCount(); i++) {
                String key = String.format("%d-%s", i, BUNDLE_KEY);
                outState.putParcelable(key, mRecyclerAdapter.get(i));
            }
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        mLogger.d("ListFragment onStop()");
        NotificationManager.getInstance().removeListener(Channel.calendar, onNewMessage);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        mLogger.d("ListFragment onDestroy()");
        NotificationManager.getInstance().removeListener(Channel.calendar, onNewMessage);
        super.onDestroy();
    }

    @Override
    public void onItemRemoved(CalendarActionItemModel model) {
        Call<DeleteResponseModel> request = mService.deleteActionItem(model.Id);
        request.enqueue(new Callback<DeleteResponseModel>() {
            @Override
            public void onResponse(Call<DeleteResponseModel> call, Response<DeleteResponseModel> response) {
                mLogger.d("Record Deleted: %d", response.body().Id);
                if(mRecyclerAdapter.getItemCount() == 0){
                    displayNoData(true);
                }
                //displaySnack("Record deleted from server!");
            }

            @Override
            public void onFailure(Call<DeleteResponseModel> call, Throwable t) {
                displaySnack("Unable to delete record from server!");
            }
        });
    }

    @Override
    public void onItemClicked(View view, int position) {
        DetailFragment fragment = new DetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(DetailFragment.BUNDLE_KEY, mRecyclerAdapter.get(position));
        fragment.setArguments(bundle);
        AppFabricBase.getBus().post(new FragmentChangeEvent(fragment, DetailFragment.FRAGMENT_TAG));
    }

    //region - Listeners
    private View.OnClickListener fabOnClick = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            AppFabricBase.getBus().post(new FragmentChangeEvent(new DetailFragment(), DetailFragment.FRAGMENT_TAG));
        }
    };

    private Callback<List<CalendarActionItemModel>> listCallback = new Callback<List<CalendarActionItemModel>>() {

        @Override
        public void onResponse(Call<List<CalendarActionItemModel>> call, Response<List<CalendarActionItemModel>> response) {
            if(response != null && response.body() != null && response.body().size() > 0) {

                mRecyclerAdapter.clear(); //CLEARING ADAPTER HACK

                for (int i = 0; i < response.body().size(); i++) {
                    addItem(response.body().get(i));
                }

                displayNoData(false);
            } else {
                mLogger.w("RSP Body Null! [%d]", response.code());
                displayNoData(true);
            }
        }

        @Override
        public void onFailure(Call<List<CalendarActionItemModel>> call, Throwable t) {
            mLogger.e("Failure calling Calendar Service", t);
        }
    };
    //endregion

    private void addItem(final CalendarActionItemModel model) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                mRecyclerAdapter.addAndNotify(model);
            }
        });
    }

    private void handleSyncEvent(final CalendarSyncEvent event){
        final String message;

        if(event.Type.equals("Insert")){
            //REFRESH ENTIRE LIST
            Call<List<CalendarActionItemModel>> result = mService.listActionItems();
            result.enqueue(listCallback);
            message = "New Record Added!";
        } else if(event.Type.equals("Delete")){
            //REMOVE SINGLE ITEM FROM LIST

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(mRecyclerAdapter != null){
                        for(int i = 0; i < mRecyclerAdapter.getItemCount(); i++){
                            if(mRecyclerAdapter.get(i).Id == event.Id){
                                mRecyclerAdapter.remove(i);
                                break;
                            }
                        }
                    }
                }
            });

            message = "Record Deleted!";
        } else {
            message = "Unknown Event Type!";
        }

        displaySnack(message);
    }

    private void displaySnack(final String message){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });
    }

    private void displayNoData(boolean visible){
        if(visible){
            mRecyclerView.setVisibility(View.GONE);
            mTextView.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            mTextView.setVisibility(View.GONE);
        }
    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {

        @Override
        public void call(final Object... args) {
            mLogger.d(String.format("Received Message: %s", args[0]));
            CalendarSyncEvent event = new Gson().fromJson(args[0].toString(), CalendarSyncEvent.class);
            handleSyncEvent(event);
        }
    };
}
