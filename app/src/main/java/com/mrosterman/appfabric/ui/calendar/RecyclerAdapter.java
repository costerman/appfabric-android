package com.mrosterman.appfabric.ui.calendar;

import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mrosterman.appfabric.R;
import com.mrosterman.appfabric.model.CalendarActionItemModel;
import com.mrosterman.appfabric.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by costerman on 4/20/16.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private static final int PENDING_REMOVAL_TIMEOUT = 3000; // 3sec

    private List<CalendarActionItemModel> mItemsPendingRemoval = new ArrayList<>();
    private int mLastInsertedIndex = 0; // so we can add some more mItems for testing purposes
    private boolean mUndoOn = true; // is undo on, you can turn it on from the toolbar menu

    private Handler mHandler = new Handler(); // hanlder for running delayed runnables
    private HashMap<CalendarActionItemModel, Runnable> mPendingRunnables = new HashMap<>(); // map of mItems to pending runnables, so we can cancel a removal if need be

    private List<CalendarActionItemModel> mData = new ArrayList<>();

    private static AdapterListener mAdapterListener;

    public RecyclerAdapter(){}
    public RecyclerAdapter(List<CalendarActionItemModel> data, AdapterListener listener){
        this.mData = data;
        this.mAdapterListener = listener;
    }

    public void setData(List<CalendarActionItemModel> data) {
        this.mData = data;
    }

    public void setItemRemovedListener(AdapterListener listener){
        mAdapterListener = listener;
    }

    public List<CalendarActionItemModel> getData(){
        return this.mData;
    }

    public void add(CalendarActionItemModel model){
        mData.add(model);

    }

    public void addAndNotify(CalendarActionItemModel model){
        add(model);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        CalendarActionItemModel item = mData.get(position);
        if (mItemsPendingRemoval.contains(item)) {
            mItemsPendingRemoval.remove(item);
        }
        if (mData.contains(item)) {
            mData.remove(position);
            notifyItemRemoved(position);
        }
        if(mAdapterListener != null){
            mAdapterListener.onItemRemoved(item);
        }
    }

    public CalendarActionItemModel get(int i){
        return mData.get(i);
    }

    public void clear(){
        mData.clear();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final RecyclerAdapter adapter = this;

        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.calendar_list_item, parent, false);

        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position, List<Object> payloads) {

        //Populate holder
        setTextFields(holder, position);

        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        ViewHolder viewHolder = (ViewHolder)holder;
        final CalendarActionItemModel item = mData.get(position);

        if (mItemsPendingRemoval.contains(item)) {
            // we need to show the "undo" state of the row
            viewHolder.itemView.setBackgroundColor(Color.RED);
            viewHolder.mCalendarListItemLayout.setVisibility(View.GONE);
            viewHolder.mButton.setVisibility(View.VISIBLE);
            viewHolder.mButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // user wants to undo the removal, let's cancel the pending task
                    Runnable pendingRemovalRunnable = mPendingRunnables.get(item);
                    mPendingRunnables.remove(item);
                    if (pendingRemovalRunnable != null) mHandler.removeCallbacks(pendingRemovalRunnable);
                    mItemsPendingRemoval.remove(item);
                    // this will rebind the row in "normal" state
                    notifyItemChanged(mData.indexOf(item));
                }
            });
        } else {
            // we need to show the "normal" state
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mAdapterListener.onItemClicked(v, position);
                }
            });
            viewHolder.itemView.setBackgroundColor(Color.WHITE);
            viewHolder.mCalendarListItemLayout.setVisibility(View.VISIBLE);
            setTextFields(viewHolder, position);
            viewHolder.mButton.setVisibility(View.GONE);
            viewHolder.mButton.setOnClickListener(null);
        }
    }

    private void setTextFields(ViewHolder holder, int position){

        if(mData.get(position) != null) {
            if (holder.mActionItemNameText != null)
                holder.mActionItemNameText.setText(mData.get(position).ActionItemName);

            if (holder.mActionItemDescText != null)
                holder.mActionItemDescText.setText(mData.get(position).ActionItemDescription);

            if (holder.mCalendarCategoryText != null)
                holder.mCalendarCategoryText.setText(mData.get(position).CalendarCategory);

            if (holder.mDueDateText != null)
                holder.mDueDateText.setText(mData.get(position).ActionItemDueDate);

            if (holder.mOwnersText != null)
                holder.mOwnersText.setText(StringUtil.concatArray(mData.get(position).PrimaryOwners));
        }
    }

    public void setUndoOn(boolean undoOn) {
        this.mUndoOn = undoOn;
    }

    public boolean isUndoOn() {
        return mUndoOn;
    }

    public void pendingRemoval(int position) {
        final CalendarActionItemModel item = mData.get(position);
        if (!mItemsPendingRemoval.contains(item)) {
            mItemsPendingRemoval.add(item);
            // this will redraw row in "undo" state
            notifyItemChanged(position);
            // let's create, store and post a runnable to remove the item
            Runnable pendingRemovalRunnable = new Runnable() {
                @Override
                public void run() {
                    remove(mData.indexOf(item));
                }
            };
            mHandler.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT);
            mPendingRunnables.put(item, pendingRemovalRunnable);
        }
    }



    public boolean isPendingRemoval(int position) {
        CalendarActionItemModel item = mData.get(position);
        return mItemsPendingRemoval.contains(item);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mCalendarListItemLayout;
        Button mButton;
        TextView mActionItemNameText;
        TextView mActionItemDescText;
        TextView mCalendarCategoryText;
        TextView mDueDateText;
        TextView mOwnersText;

        public ViewHolder(View itemView) {
            super(itemView);

            //TODO: Implement all view objects
            mCalendarListItemLayout = (LinearLayout) itemView.findViewById(R.id.calendar_list_item_layout);
            mButton = (Button) itemView.findViewById(R.id.calendar_list_item_undo_button);
            mActionItemNameText = (TextView) itemView.findViewById(R.id.action_item_name_item_label);
            mActionItemDescText = (TextView) itemView.findViewById(R.id.action_item_desc_item_label);
            mCalendarCategoryText = (TextView) itemView.findViewById(R.id.calendar_category_item_label);
            mDueDateText = (TextView) itemView.findViewById(R.id.due_date_item_label);
            mOwnersText = (TextView) itemView.findViewById(R.id.owners_item_label);

        }
    }

    public interface AdapterListener {
        void onItemRemoved(CalendarActionItemModel model);
        void onItemClicked(View view, int position);
    }
}
