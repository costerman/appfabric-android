package com.mrosterman.appfabric.ui.common;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;

/**
 * Created by carl on 4/20/16.
 */
public class OnSwipeTouchListener implements View.OnTouchListener {

    private GestureDetector mGestureDetector;
    private Context mContext;
    private ListView mListView;

    public OnSwipeTouchListener(Context context, ListView listView){
        mContext = context;
        mListView = listView;
        mGestureDetector = new GestureDetector(mContext, new OnGestureListener());
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return mGestureDetector.onTouchEvent(event);
    }

    private void onSwipeRight(int pos){

    }

    private void onSwipeLeft(){

    }

    private final class OnGestureListener extends GestureDetector.SimpleOnGestureListener{

        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            float distanceX = e2.getX() - e1.getX();
            float distanceY = e2.getY() - e1.getY();
            if (Math.abs(distanceX) > Math.abs(distanceY) && Math.abs(distanceX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                if (distanceX > 0) {
                    onSwipeRight(getPostion(e1));
                } else {
                    onSwipeLeft();
                }
                return true;
            }
            return false;
        }

        private int getPostion(MotionEvent e1) {
            return mListView.pointToPosition((int) e1.getX(), (int) e1.getY());
        }
    }
}
