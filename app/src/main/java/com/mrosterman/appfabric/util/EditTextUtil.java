package com.mrosterman.appfabric.util;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.IdRes;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.mrosterman.appfabric.R;

/**
 * Created by carl on 4/19/16.
 */
public class EditTextUtil {

    public static synchronized View.OnFocusChangeListener hideKeyboardOnFocusChange(final Activity activity, @IdRes final int resId){
        return new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(v.getId() == resId && !hasFocus) {
                    InputMethodManager imm =  (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                }
            }
        };
    }
}
