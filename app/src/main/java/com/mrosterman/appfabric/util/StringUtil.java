package com.mrosterman.appfabric.util;

import java.util.Arrays;

/**
 * Created by carl on 4/19/16.
 */
public class StringUtil {

    public static synchronized String concatArray(String[] array){
        String result = "";
        if (array != null && array.length > 0) {
            for (int i = 0; i < array.length; i++) {
                result = StringUtil.concat(result, array[i]);
            }
        }
        return result;
    }

    private static synchronized String concat(String a, String b){
        if(a.length() > 0)
            a += ",";
        a += b;
        return a;
    }

    public static synchronized String[] delimitedToArray(String delim, String data){
        String[] result = data.split(delim);
        return result;
    }
}
